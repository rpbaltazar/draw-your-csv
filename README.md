# Needs

- python
- pygame (`pip install pygame`)

# Run

- ensure you have the csv file in the expected format or update the code to match your format
- `python draw-map.py` should open a window and have a moving circle acording to your data
