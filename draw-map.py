import pygame
import sys
import csv

screen = pygame.display.set_mode((1400,800))
pygame.display.flip()
white = (255,255,255)
black = (0,0,0)

with open('car1.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile)
    rows = [r for r in reader]

num_of_lines = len(rows)
index = 1 # 0 contains the headers

while (True):
    screen.fill(white)
    x = int(float(rows[index][1]))
    y = int(float(rows[index][2]))
    pygame.draw.circle(screen, black, (x,y), 10, 0)

    print x,y

    index+=1;
    if index == num_of_lines:
        index = 1

    pygame.display.update()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
             pygame.quit()
             sys.exit()
